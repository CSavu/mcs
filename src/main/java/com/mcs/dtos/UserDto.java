package com.mcs.dtos;

import com.mcs.entities.Role;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UserDto {
    private Integer id;
    private String username;
    private Role role;
}