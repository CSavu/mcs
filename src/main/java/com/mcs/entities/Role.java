package com.mcs.entities;

public enum Role {
    REGULAR_USER("REGULAR_USER"),
    ADMIN("ADMIN");

    private String key;

    Role(String key) {
        this.key = key;
    }

    public String getRoleKey() {
        return key;
    }
}
