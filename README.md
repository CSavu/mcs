# MCS (Messaging and Communication Service)

## Overview
MCS (Messaging and Communication Service) is a messaging service used for user-admin communication within the Energy Management System. It provides real time messaging and notifications.